/*
 *  Cache simulation project
 *  Class UCR IE-521
 */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include "../include/debug_utilities.h"
#include "../include/L1cache.h"
#include <cmath>

#define LOAD 0
#define STORE 1
#define KB 1024
#define ADDRSIZE 32
using namespace std;

int field_size_get(struct cache_params cache_params,
                   struct cache_field_size *field_size)
{
   field_size->offset = log2(cache_params.block_size);
   field_size->idx    = log2(cache_params.size/(cache_params.block_size*cache_params.asociativity));
   field_size->tag    = 32 - field_size->idx - field_size->offset;
   return OK;
}

void address_tag_idx_get(long address,
                         struct cache_field_size field_size,
                         int *idx,
                         int *tag)
{
      int offset_mask = pow(2,field_size.offset) - 1;
      int index_mask  = pow(2,field_size.idx) - 1;
      int tag_mask    = pow(2,field_size.tag) - 1;
      index_mask = index_mask << field_size.offset;
      tag_mask = tag_mask << (field_size.offset + field_size.idx);
      *idx = address & index_mask;
      *idx = *idx >> field_size.offset;
      *tag = address & tag_mask;
      *tag = *tag >> (field_size.offset + field_size.idx);
}

bool power2(int num){
   return !((bool)(num & (num-1)));
}

void cache_update(entry* cache_blocks, int tag, int loadstore, int rp_value){

   if(loadstore == STORE){
      cache_blocks->dirty = true;
   }
   else{
      if(cache_blocks->tag != tag){
         cache_blocks->dirty = false;
      }
   }

   cache_blocks->tag      = tag;
   cache_blocks->valid    = true;
   cache_blocks->rp_value = rp_value;
}

int srrip_update(entry* cache_blocks, 
                  int associativity, 
                  int rrip_bits, 
                  int set_loc,
                  int flag_hm){
 
   int peso = 0;
   int victim_adrs = 0;
   int victim_found = 0;
   int tag_now = 0; 
   if(flag_hm == 0){//MISS
      while(!victim_found){//loop que se mantiene hasta que encuentre una victima
         for (int j=0; j<associativity; j++){//barrido de todos los ways buscando un valor con peso victimizable
            victim_adrs=set_loc+j;
            peso = cache_blocks[victim_adrs].rp_value;
            //printf("Way_%i's RP_value_now: %i\n",j, peso);
            if(peso == rrip_bits){//si se cumple esto se encontro una victima
               victim_found=1;
               return victim_adrs;   
            }
         }
         if(victim_found == 0){
            for (int i=0; i<associativity; i++){//si no encuentra un valor victimizable se aumenta el peso de todos
               victim_adrs=set_loc+i;
               cache_blocks[victim_adrs].rp_value++;
            }
         }
      }
   }
   else {//HIT
      return OK;            
   }  
   return OK; 
}

void print_ways(entry* cache_blocks, 
                  int associativity, 
                  int set_loc){
   int abc = 0;
   for (int j=0; j<associativity; j++){//barrido de todos los ways
            abc = set_loc+j;
            printf("------------------------------------------\n");
            printf("|  Way |  Tag  |  RP value  |\n");
            printf("|  %i  |  %i  |  %i  |\n",j , cache_blocks[abc].tag, cache_blocks[abc].rp_value);
         }   
}

int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
   if(idx < 0 || tag < 0 || associativity < 1 || !power2(associativity) || cache_blocks == nullptr || result == nullptr){
      return PARAM;
   }

   int flag_hm = 0;//flag de hit o miss, hit = 1
   int set_loc = 0;//indica el puntero al set correcto
   int rrip_bits = 0; //bits rrip
   int way_value = 0;
   int tag_now = 0;
   int valid_now = 0;
   int dirty_now = 0;
   int victim = 0;
   //set_loc = idx * associativity;
   if (debug){ 
      printf("VALORES INICIALES DE LA TABLA:\n"); 
      print_ways(cache_blocks, associativity, set_loc);      
   }
   if (associativity < 3 ) {
      rrip_bits = 1;//asociatividad de 1 o 2 se ponen bits de rrip=1 
   }
   else {
      rrip_bits = 3;//asociatividad de 3 o mas se ponen bits de rrip=2
   }
   //este for revisa todos los ways buscando por datos invalidos o tags iguales
   for (int i = 0; i<associativity; i++){
      way_value = set_loc + i;//puntero de way
      if(debug){    
         printf("i: %i\n", i);
         printf("set_loc: %i\n", set_loc);
         printf("way_value: %i\n", way_value);
      }
      tag_now = cache_blocks[way_value].tag;//tag guardado en el way
      valid_now = cache_blocks[way_value].valid;//valid en el way
      dirty_now = cache_blocks[way_value].dirty;//dirty en el way 
      if(debug){    
         printf("Tag_now: %i\n", tag_now);
         printf("Tag: %i\n", tag);
         //printf("\nValid_now: %i\n", valid_now);
      }
      if (valid_now == 0){//si valid = 0 entonces se debe reemplazar el dato inmediatamente
         result->miss_hit = (loadstore == LOAD)? MISS_LOAD:MISS_STORE;
         cache_update(&cache_blocks[i], tag,loadstore,rrip_bits-1);
         
         return 0;     
      }
      else {
         if (tag_now == tag){//indica condicion de HIT    
            if(debug){    
            printf("Hubo hit en el way_%i\n",way_value);
            printf("Tag_now: %i\n", tag_now);
            printf("Tag: %i\n", tag);
         }
            flag_hm = 1;   //se coloca bandera de hit en 1
            result->miss_hit = (loadstore == false)? HIT_LOAD:HIT_STORE;
            cache_update(&cache_blocks[i], tag,loadstore,0);            
            if(debug){
               printf("ESTADO DE LA TABLA DESPUES DE HIT\n");
               print_ways(cache_blocks, associativity, set_loc);    
            }
            return OK;//se termina la funcion
         }
      }   
   }
   if(flag_hm==0){
   //si termino el barrido y no hubo hit o invalid entonces es un miss y se debe actualizar.
      result->miss_hit = (loadstore == LOAD)? MISS_LOAD:MISS_STORE;
      victim=srrip_update(cache_blocks,associativity,rrip_bits,set_loc,flag_hm);//se cambian los valores de srrip;
      result->evicted_address = cache_blocks[victim].tag;
      result->dirty_eviction  = cache_blocks[victim].dirty;
      cache_update(&cache_blocks[victim], tag,loadstore,rrip_bits-1);
      if(debug){
         printf("ESTADO DE LA TABLA DESPUES DE MISS\n");
         print_ways(cache_blocks, associativity, set_loc);      
         }
            
      return OK;//la funcion termina
   }
   return ERROR;
}

void lru_update(entry* cache_block, int associativity, int thd){

   for(size_t i = 0; i < associativity; i++){

      if(cache_block[i].rp_value < thd){ // if a rp_value is less than thd
         cache_block[i].rp_value++;   // rp_value increase by one
      } else {
         if(cache_block[i].rp_value > associativity) {
            cache_block[i].rp_value = 0;
         }
      }
   }
}


int lru_replacement_policy ( int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
  if(idx < 0 || tag < 0 || associativity < 1 || !power2(associativity) || cache_blocks == nullptr || result == nullptr){

  return PARAM;
  }

   if(debug)printf("\nAddress: %i\n", tag);

   int free = -1;
   int victim = -1;

   for(size_t i = 0; i < associativity; i++){

      if(cache_blocks[i].valid == true){

         //ML_Result_1
         if(cache_blocks[i].tag == tag){

            int thd = cache_blocks[i].rp_value; // rp_value of the most recenlty used act like threshold
            cache_update(&cache_blocks[i], tag,loadstore,-1); // set the dirty bit and update the rp_value
            lru_update(cache_blocks,associativity,thd); // update the rp_value for all other ways
            
            result->miss_hit = (loadstore == LOAD)? HIT_LOAD:HIT_STORE; //RPL_1
            result->evicted_address = -1; // no address eviction was need
            if(debug){
               print_way_info(idx, associativity, cache_blocks);
               printf("Operation Result: \n Miss_Hit: %i,\n Dirty_Eviction: %i,\n Evicted_address: %i\n", result->miss_hit, result->dirty_eviction, result->evicted_address);
            }
            return OK;
         }

         if(cache_blocks[i].rp_value == associativity-1){
            victim = i; // victim will be the way with higher rp_value, it will be equal to the number of ways
         }
      }
      else{
         free = i; // if there is a way with no valid data that way is free
      }

   }
   result->miss_hit = (loadstore == LOAD)? MISS_LOAD:MISS_STORE; // no hit operation was reached
   result->evicted_address = -1; // no address eviction was need
   if(free != -1){ // there is a free way so no victim was need
      cache_update(&cache_blocks[free], tag,loadstore,-1); // set the new data in the free way
      lru_update(cache_blocks,associativity,associativity-1); // update the rp_value for all other ways
      
      if(debug){
         print_way_info(idx, associativity, cache_blocks);
         printf("Operation Result: \n Miss_Hit: %i,\n Dirty_Eviction: %i,\n Evicted_address: %i\n", result->miss_hit, result->dirty_eviction, result->evicted_address);
      }
      return OK;
    }
   else{  // there is no free way so one way will be evicted

      result->evicted_address = cache_blocks[victim].tag;
      result->dirty_eviction  = cache_blocks[victim].dirty;

      int thd = cache_blocks[victim].rp_value;
      cache_update(&cache_blocks[victim],tag,loadstore,-1); // set the new parameters in the evicted way
      lru_update(cache_blocks,associativity,thd); // update the rp_value for all other ways

      if(debug){
         print_way_info(idx, associativity, cache_blocks);
         printf("Victimized Way: %i.\n", victim);
         printf("Operation Result: \n Miss_Hit: %i,\n Dirty_Eviction: %i,\n Evicted_address: %i\n", result->miss_hit, result->dirty_eviction, result->evicted_address);
      }
      return OK;
   }
   return ERROR;
}


void nru_update(entry* cache_blocks, int associativity){
   for(size_t i = 0; i < associativity; i++){
      cache_blocks[i].rp_value = 1;
   }
}

int nru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug)
{

if(idx < 0 || tag < 0 || associativity < 1 || !power2(associativity) || cache_blocks == nullptr || operation_result == nullptr){

  return PARAM;
  }

   if(debug)printf("\nAddress: %i\n", tag);

   int free = -1;
   int victim = -1;

   for(size_t i = 0; i < associativity; i++){

      if(cache_blocks[i].valid == true){

         if(cache_blocks[i].tag == tag){

            operation_result->miss_hit = (loadstore == LOAD)? HIT_LOAD:HIT_STORE;
            cache_update(&cache_blocks[i], tag, loadstore, 0);
            if(debug){
               print_way_info(idx, associativity, cache_blocks);
               printf("Operation Result: \n Miss_Hit: %i,\n Dirty_Eviction: %i,\n Evicted_address: %i\n", operation_result->miss_hit, operation_result->dirty_eviction, operation_result->evicted_address);
            }

            return OK;
         }
         if(cache_blocks[i].rp_value == 1){
            victim = i;
         }
      }
      else{
         free = i;
      }
   }

   operation_result->miss_hit = (loadstore == LOAD)? MISS_LOAD:MISS_STORE;
   if(free != -1){

      cache_update(&cache_blocks[free], tag, loadstore, 0);
      if(debug){
         print_way_info(idx, associativity, cache_blocks);
         printf("Operation Result: \n Miss_Hit: %i,\n Dirty_Eviction: %i,\n Evicted_address: %i\n", operation_result->miss_hit, operation_result->dirty_eviction, operation_result->evicted_address);
         }
      return OK;
   }
   else{
      if(victim == -1){
         nru_update(cache_blocks, associativity);
         victim = associativity-1;
      }

      operation_result->evicted_address = cache_blocks[victim].tag;
      operation_result->dirty_eviction  = cache_blocks[victim].dirty;
      cache_update(&cache_blocks[victim],tag,loadstore,0);

      if(debug){
         print_way_info(idx, associativity, cache_blocks);
         printf("Operation Result: \n Miss_Hit: %i,\n Dirty_Eviction: %i,\n Evicted_address: %i\n", operation_result->miss_hit, operation_result->dirty_eviction, operation_result->evicted_address);
         printf("Victimized Way: %i.\n", victim);
         }

      return OK;
   }

   return ERROR;


}

