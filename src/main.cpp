#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <sstream>
#include "../include/L1cache.h"
#include "../include/debug_utilities.h"

#define MISS_PENALTY 20
#define HIT_TIME 1

using namespace std; 
/* Helper funtions */
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}

int main(int argc, char * argv []) {
	/* Program Variables */
  	char numeral;
  	int ls;
  	long dir;
  	int ic;
	int cache_policy; 
	/*Variables de resultados*/
	long cpu_time; 
	float amat_time;
	float cpi;
	long amat_cycles;
	float overall_miss_rate;
	float read_miss_rate;
	float mem_ref_instruction;
	int total_dirty_evictions = 0;
	int load_misses = 0;
	int store_misses = 0;
	long total_misses = 0;
	int load_hits= 0;
	int store_hits= 0;
	long total_hits= 0;
	long total_IC = 0;
	long total_memref = 0;
	
	/*Tag and index*/
	int idx; 
	int tag;
	int number_of_entries;
  	/*Estructura Parametros del cache*/
  	cache_params cache_values; 
	/*Linea de entrada */
  	string line;


  	//printf("Do something :), don't forget to keep track of execution time\n");
  	/* Parse argruments */
  	if (argc > 1) {
  		cache_values.size = atoi(argv[4]);
  		cache_values.block_size = atoi(argv[6]);
  		cache_values.asociativity = atoi(argv[2]);
  		cache_policy = atoi(argv[8]);
  	}
	/*Esctructura del Cache en caso de solo ser L1*/
	number_of_entries = cache_values.size/cache_values.block_size;
	cout << "num entries %d \n" << number_of_entries << endl;
	entry cache[number_of_entries];
	operation_result op_result;
  	/*Estructura con Valores de indice, tag y offset del cache*/
  	cache_field_size cache_info;
	/*Funciona genera la cantida de bits para index, offset y tag*/
  	field_size_get(cache_values, &cache_info);
  	/* Get trace's lines and start your simulation */
  	while(getline(cin, line)) {
		/*Se parsea la linea de entrada */
  		std::stringstream ss(line);
		/* Se obtienen los valores de entrada y se guardan */
  		ss >> numeral >> std::hex >>ls >> dir >> ic; 
		/* Se obtiene el indice y tag de cada direccion ingresada*/
  		address_tag_idx_get(dir, cache_info, &idx, &tag);
		if(cache_policy == 0) {
			lru_replacement_policy(idx,tag, cache_values.asociativity, ls, cache, &op_result, false);
		} 
		if(cache_policy == 1) {
			nru_replacement_policy(idx,tag, cache_values.asociativity, ls, cache, &op_result, false);
		}
		if(cache_policy == 2) {
			srrip_replacement_policy(idx,tag, cache_values.asociativity, ls, cache, &op_result, false);
		}
		total_memref += 1;
		total_IC += ic;
		if(op_result.miss_hit == MISS_LOAD) {
			load_misses += 1;
		}
		if(op_result.miss_hit == MISS_STORE) {
			store_misses += 1;
		}
		if(op_result.miss_hit == HIT_LOAD) {
			load_hits += 1;
		}
		if(op_result.miss_hit == HIT_STORE) {
			store_hits += 1;
		}
		if(op_result.dirty_eviction) {
			total_dirty_evictions += 1;
		}
  	}
	/* Calculos de estadisticas de salida */
	//printf("memref %ld, ic %ld\n", total_memref, total_IC);
	/*Miss Rate en general*/
	overall_miss_rate = (float)(load_misses+store_misses)/(float)total_memref;
	//cout << "overall miss rate " << overall_miss_rate << endl;
	/*AMAT time*/
	amat_time = HIT_TIME + overall_miss_rate*MISS_PENALTY;
	/*Miss rate en read, es igual a solo tomer en cuenta los misses y las instrucciones que referencian memoria*/
	read_miss_rate = (float)load_misses/(float)total_memref;
	/*Como todas las instr duran 1clock cycle excepto loads cuando hay miss cpi es igual a la probabilidad de eso.  */
	cpi = read_miss_rate*(1+MISS_PENALTY); 
	//cout << "cpi" << cpi << endl;
	/*Mem_access/Instr es igual a la fraccion de referencias a memoria en el total de instrucciones*/
	mem_ref_instruction = (float)total_memref/(float)total_IC;
	//cout << "mem_ref_ins" << mem_ref_instruction << endl;
	/*Cpu_time = IC*(CPI + MISS_RATE * MEM_ACCESS/INSTR * MISS_PENALTY) */
	cpu_time = total_IC*(cpi + overall_miss_rate*mem_ref_instruction*MISS_PENALTY);

  	 
  	/* Print cache configuration */
	cout << "-----------------------------------------" << endl;
	cout << " Cache Parameters                        " << endl;
	cout << "-----------------------------------------" << endl;
	cout << " Cache Size (KB)               " << cache_values.size << endl;
	cout << " Cache Associativity           " << cache_values.asociativity << endl;
	cout << " Cache Block Size (bytes)      " << cache_values.block_size << endl;
  	/* Print Statistics */
	cout << "-----------------------------------------" << endl;
	cout << " Simulation Results" << endl;
	cout << "-----------------------------------------" << endl;
	cout << " CPU time (cycles):             " << cpu_time << endl;
	cout << " AMAT (cycles):                 " << amat_time <<endl;
	cout << " Overall miss rate:             " << overall_miss_rate << endl;
	cout << " Read miss rate:                " << read_miss_rate << endl;
	cout << " Dirty evictions:               " << total_dirty_evictions << endl;
	cout << " Load Misses:                   " << load_misses << endl;
	cout << " Store Misses:                  " << store_misses << endl;
	cout << " Total Misses:                  " << store_misses+load_misses << endl;
	cout << " Load Hits:                     " << load_hits  << endl;
	cout << " Store Hits:                  	" << store_hits << endl;
	cout << " Total Hits:                    " << store_hits+load_hits << endl;
	cout << "-----------------------------------------" << endl;
	
	return 0;
}
