/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define STORE 1
#define LOAD 0 
#define KB 1024
#define ADDRSIZE 32
#define u_int unsigned int
using namespace std;


int l1_l2_entry_info_get (const struct cache_params l1_params, 
						  const struct cache_params l2_params,
						  long address,
						  struct l1_l2_entry_info* l1_l2_info,
						  bool debug) 
{
	int l1_tag_size;
	int l2_tag_size;
	int l1_idx_size;
	int l2_idx_size;
	int l1_offset_size;
	int l2_offset_size;
	/* Calculate length of parameters for each cache*/
	l1_offset_size = log2(l1_params.block_size);
	l2_offset_size = log2(l2_params.block_size); 
	l1_idx_size = log2(l1_params.size/(l1_params.block_size*l1_params.asociativity));
	l2_idx_size = log2(l2_params.size/(l2_params.block_size*l2_params.asociativity));
	l1_tag_size = 32 - l1_offset_size - l1_idx_size;
	l2_tag_size = - l2_offset_size - l2_idx_size;
	/*Debug option*/
	if(debug) {
		printf("l1 tag size %d, l2 tag size %d, l1 index size %d, l2 index size %d, l1 asociativity %d, l2 asociativity %d\n",
			 l1_tag_size, l2_tag_size, l1_idx_size, l2_idx_size, l1_params.asociativity, l2_params.asociativity); 
	}
	/* Creation of masks to access cache info*/
	int index_mask = pow(2,l1_idx_size) - 1; 
	/* l1 index calculation*/
	int l1_index = address & (index_mask >> l1_offset_size); 
	index_mask = pow(2,l2_idx_size) - 1;
	int l2_index = address & (index_mask >> l2_offset_size); 
	/*l2 index calculation*/ 
	u_int tag_mask = pow(2,l1_tag_size) -1;
	/*l1 tag calculation*/
	u_int l1_tag = address & (tag_mask >> (l1_idx_size + l1_offset_size));
	tag_mask = pow(2,l2_tag_size) - 1;
	/*l2 tag calculation*/
	u_int l2_tag = address & (tag_mask >> (l2_idx_size + l2_offset_size));


	if(debug) {
		printf("l1 tag %d, l1 index %d, l2 tag %d, l2 index %d", 
		l1_tag, l1_index, l2_tag, l2_index);
	}  
	/*Save data to return structs*/
	l1_l2_info->l1_tag = l1_tag;
	l1_l2_info->l1_idx = l1_index;
	l1_l2_info->l1_associativity = l1_params.asociativity;
	l1_l2_info->l2_tag = l2_tag;
	l1_l2_info->l1_idx = l2_index;
	l1_l2_info->l2_associativity = l2_params.asociativity;

	return OK;
}

void l1_l2_lru_update(entry* cache_block, int associativity, int thd, int idx) {
	for (int i = 0; i < associativity ; i ++) {
		if(cache_block[i].rp_value < thd) {
			cache_block[i].rp_value++;
		}
	}
}

void l1cache_hit_update(entry* cache_to_update, int tag_in, int loadstore, int set, int l1_asoc) {
	for(int i = 0 ; i < l1_asoc ; i++) {
		if(cache_to_update[i].tag == tag_in) {
			cache_to_update[i].rp_value = 0; 
			if(loadstore == STORE & cache_to_update[i].dirty == false) {
				cache_to_update[i].dirty = true;
			} else {
				cache_to_update[i].dirty = false; 
			}
		}
	}
}


int cacheupdate(entry* cache, int tag_in, int loadstore, int rp_value, int set, int cache_asoc) {
	int evict_way;
	for(int i = 0 ; i < cache_asoc ; i++ ) {
		if(cache[i].valid == 0) {
			cache[i].rp_value = 0;
			cache[i].valid = true; 
			cache[i].tag = tag_in;
			if(loadstore == STORE) {
				cache[i].dirty = true;
			} else {
				cache[i].dirty = false; 
			}
			return 1;

		} else {
			if(cache[i].rp_value == cache_asoc -1) {
				evict_way = i;
			}
		}
	} 
	return 1;
}
int victimize (entry* cache, int tag_in, int loadstore, int set, int victimloc) {
	cache[victimloc].rp_value = 0;
	cache[victimloc].valid = true; 
	cache[victimloc].tag = tag_in; 
	if(loadstore == STORE) {
		cache[victimloc].dirty = true;
	} else {
		cache[victimloc].dirty = false; 
	}
	return 1;

}
int update_l2_hit(entry* l2cache, int tag_find, int loadstore, int set, int l2_asoc) {
	for(int i = 0; i < l2_asoc ; i++) {
		if(l2cache[i].tag == tag_find) {
			l2cache[i].rp_value = 0;
			if(loadstore == STORE & l2cache[i].dirty != 1) {
				l2cache[i].dirty = true;
			} else {
				l2cache[i].dirty = false; 
			}
			return 1;
		}
	}
	return 0;
}


int lru_replacement_policy_l1_l2(const l1_l2_entry_info *l1_l2_info,
				 bool loadstore,
				 entry* l1_cache_blocks,
				 entry* l2_cache_blocks,
				 operation_result* l1_result,
				 operation_result* l2_result,
				 bool debug) 
{	
	int thd;
	int l1_victim;
	int l2_victim;
	/*check correct input parameters*/
	if(l1_l2_info->l1_idx < 0 || l1_l2_info->l1_tag < 0 || l1_l2_info->l1_associativity < 1 || !power2(l1_l2_info->l1_associativity) || l1_cache_blocks == nullptr || l1_result == nullptr ||
	   l1_l2_info->l2_idx < 0 || l1_l2_info->l2_tag < 0 || l1_l2_info->l2_associativity < 1 || !power2(l1_l2_info->l2_associativity) || l2_cache_blocks == nullptr || l2_result == nullptr ) {
		   
  		return PARAM;
  	} 
	/*set to check, in each cache */
	int l1_set = l1_l2_info->l1_idx * l1_l2_info->l1_associativity;
	int l2_set = l1_l2_info->l2_idx * l1_l2_info->l2_associativity;
	//Must be zero for tests
	l1_set = 0;
	l2_set = 0;
	
	/*check l1 cache first then if not found check l2*/
	for(int i = 0; i < l1_l2_info->l1_associativity ; i++) {
		/*if valid is cero then theres space and its a miss*/
		if(l1_cache_blocks[i].valid == 0) {
			
			l1_l2_lru_update(l1_cache_blocks, l1_l2_info->l1_associativity, l1_l2_info->l1_associativity-1, l1_set);
			cacheupdate(l1_cache_blocks, l1_l2_info->l1_tag, loadstore, 0, l1_set, l1_l2_info->l1_associativity);
			/*Also written to l2 cache since its writethrough*/
			l1_l2_lru_update(l2_cache_blocks, l1_l2_info->l2_associativity, l1_l2_info->l2_associativity-1, l2_set);
			cacheupdate(l2_cache_blocks, l1_l2_info->l2_tag, loadstore, 0, l2_set, l1_l2_info->l2_associativity);
			l1_result->miss_hit = (loadstore == LOAD) ? MISS_LOAD:MISS_STORE;
			l2_result->miss_hit = l1_result->miss_hit; 

			if(debug) {
				
			}
			return OK; 
		} else {
			if(l1_cache_blocks[l1_set +i].tag == l1_l2_info->l1_tag) {
				thd = l1_cache_blocks[l1_set + i].rp_value;
				l1_l2_lru_update(l1_cache_blocks, l1_l2_info->l1_associativity, l1_l2_info->l1_associativity - 1, l1_set);
				l1cache_hit_update(l1_cache_blocks, l1_l2_info->l1_tag, loadstore, l1_set, l1_l2_info->l1_associativity);
				// add operation results
				l1_result->miss_hit = (loadstore == LOAD) ? HIT_LOAD:HIT_STORE;
				/*if there was a hit in l1 it means theres a hit in l2 due to inclusive caches*/
				l2_result->miss_hit = (loadstore == LOAD) ? HIT_LOAD:HIT_STORE;
				l1_l2_lru_update(l2_cache_blocks,l1_l2_info->l2_associativity, l1_l2_info->l2_associativity -1, l2_set); 
				update_l2_hit(l2_cache_blocks, l1_l2_info->l2_tag, loadstore, l2_set, l1_l2_info->l2_associativity); 
				return OK; 
			} else {
				if(l1_cache_blocks[l1_set + i].rp_value >= l1_l2_info->l1_associativity -1 ) {
					l1_victim = i; 
				}
			}
		}
	}
	/*if its not on l1_cache, check l2_cache*/
	/*also victim is calculated & l1 cache set is full already by this point*/
	for(int i = 0; i < l1_l2_info->l2_associativity; i++) {
		/*if we find a valid 0 in l2_cache without finding tag find its a miss by default*/
		if(l2_cache_blocks[l2_set + i].valid == 0) {
			l1_result->miss_hit = (loadstore == LOAD) ? MISS_LOAD:MISS_STORE;
			l2_result->miss_hit = (loadstore == LOAD) ? MISS_LOAD:MISS_STORE;
			/*victim from l1 cache since its full address (tag)*/
			l1_result->evicted_address = l1_cache_blocks[l1_set + l1_victim].tag;
			l1_l2_lru_update(l1_cache_blocks, l1_l2_info->l1_associativity, l1_l2_info->l1_associativity - 1, l1_set);
			/*writes new value to l1 cache and then l2 since theyre inclusive*/
			l1_l2_lru_update(l2_cache_blocks, l1_l2_info->l2_associativity, l1_l2_info->l2_associativity - 1, l2_set);
			victimize(l1_cache_blocks, l1_l2_info->l1_tag, loadstore, 0, l1_victim);
			/*Also written to l2 cache since its writethrough*/
			l2_result->evicted_address = 0;
			cacheupdate(l2_cache_blocks, l1_l2_info->l2_tag, loadstore, 0, l2_set, l1_l2_info->l2_associativity);
			if(debug) {

			}

			return OK; 
		} else {
			if(l2_cache_blocks[l2_set + i].tag == l1_l2_info->l2_tag) {
				/*this mean a hit on l2 cache, and not on l1 cache. 
				  also means l1 cache is full so will need to be evict
				  to stay inclusive									
				*/
				/*threshold value to update l2 cache*/
				thd = l2_cache_blocks[l2_set + i].rp_value;
				/*results for l1 and l2 caches states */
				l1_result->miss_hit = (loadstore == LOAD) ? MISS_LOAD:MISS_STORE;
				l2_result->miss_hit = (loadstore == LOAD) ? HIT_LOAD:HIT_STORE;
				/*update l2 rp_values*/
				l1_l2_lru_update(l2_cache_blocks, l1_l2_info->l2_associativity, l1_l2_info->l2_associativity - 1, l2_set);
				/*update hit way rp_value*/
				update_l2_hit(l2_cache_blocks, l1_l2_info->l2_tag, loadstore, l2_set, l1_l2_info->l2_associativity);
				/*update values for ways that are not the victim in l1 cache*/
				l1_l2_lru_update(l1_cache_blocks, l1_l2_info->l1_associativity, l1_cache_blocks[l1_set+l1_victim].rp_value, l1_set);
				/*update victim way with the new value and its rp_value of 0 for lru*/
				l1_result->evicted_address = l1_cache_blocks[l1_set + l1_victim].tag;
				victimize(l1_cache_blocks, l1_l2_info->l1_tag, loadstore, 0, l1_victim);
				if(debug) {

				}
				return OK; 
			} else {
				if(l2_cache_blocks[l2_set + i].rp_value >= l1_l2_info->l2_associativity -1 ) {
					l2_victim = i;
				}
			}
		}
	}
	/*reaching here means data is not on l1 or l2 cache, must be added to both*/
	/*also means both caches sets are full*/
	/*update both caches lru*/
	l1_l2_lru_update(l1_cache_blocks, l1_l2_info->l1_associativity, l1_cache_blocks[l1_set+l1_victim].rp_value, l1_set);
	l1_l2_lru_update(l2_cache_blocks, l1_l2_info->l2_associativity, l2_cache_blocks[l2_set+l2_victim].rp_value, l2_set);
	/*save dirty value of l2 way to victimize before overrite with new value*/
	l2_result->dirty_eviction = l2_cache_blocks[l2_set + l2_victim].dirty;
	l2_result->evicted_address = l2_cache_blocks[l2_set + l2_victim].tag; 
	l1_result->evicted_address = l1_cache_blocks[l1_set + l1_victim].tag;
	/*write value on both caches on their respective victimized way for the set*/
	victimize(l1_cache_blocks, l1_l2_info->l1_tag, loadstore, 0, l1_victim);
	victimize(l2_cache_blocks, l1_l2_info->l2_tag, loadstore, 0, l2_victim);
	/*operation results*/
	
	l1_result->miss_hit = (loadstore == LOAD) ? MISS_LOAD:MISS_STORE;
	l2_result->miss_hit = (loadstore == LOAD) ? MISS_LOAD:MISS_STORE;

	if(debug) {

	}
	return OK;
}
