/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info *l1_vc_info,
    	                      	 bool loadstore,
       	                    	 entry* l1_cache_blocks,
       	                  	 entry* vc_cache_blocks,
        	                 operation_result* l1_result,
              	              	 operation_result* vc_result,
                	         bool debug)
{
    //Access to l1
	int ac_l1 = lru_replacement_policy(l1_vc_info->l1_idx, l1_vc_info->l1_tag, l1_vc_info->l1_assoc, loadstore, l1_cache_blocks, l1_result, debug);

	if (ac_l1 != OK) {  //If no successful operation was reached
        if (debug) {
            printf("Access to L1 fail. Error code: %i\n", ac_l1);
            }
        return ac_l1;   //Return the error identifier
	}
	else {
    if ((l1_result->miss_hit == MISS_STORE || l1_result->miss_hit == MISS_LOAD) && l1_result->evicted_address != -1) { //Access to vc
      if (debug) {
            printf("################################################################################################");
    		printf("L1 miss.\n");
      }

      bool vc_hit = false;
      for (int i = 0; i < l1_vc_info->vc_assoc; i++) {
        if (vc_cache_blocks[i].tag == l1_vc_info->l1_tag && vc_cache_blocks[i].valid) { //We get a hit in vc
          vc_hit = true;
          vc_result->miss_hit = (loadstore) ? HIT_STORE : HIT_LOAD;
          break;
        }
      }

      if (!vc_hit) { //No hit was reached
        vc_result->miss_hit = (loadstore) ? MISS_STORE : MISS_LOAD;
      }

      bool free_way = false;    //Free way in vc?
      int fwi;                  //Free way index
      for (int i = 0; i < l1_vc_info->vc_assoc; i++) {
        if (vc_cache_blocks[i].valid == 0) {
          free_way = true;      //If a way has no valid information then that way is free
          fwi = i;
        }
      }
      if(free_way){
        for (int i = 0; i < l1_vc_info->vc_assoc; i++) {
          if (vc_cache_blocks[i].valid) {
            vc_cache_blocks[i].rp_value++; //Update the rp_value of all ways of the vc with a FIFO replacement policy
          }
        }
        cache_update(&vc_cache_blocks[fwi], l1_result->evicted_address, loadstore, 0); //Set the new parameter for the free way in the vc
      }
      else { //We need to find a victim
        for (int i = 0; i < l1_vc_info->vc_assoc; i++) {
          vc_cache_blocks[i].rp_value++; //Update the rp_value of all ways of the vc with a FIFO replacement policy
          if (vc_cache_blocks[i].rp_value == l1_vc_info->vc_assoc) { //The victim will be the way with higher rp_value
            vc_result->dirty_eviction = vc_cache_blocks[i].dirty;
            vc_result->evicted_address = vc_cache_blocks[i].tag;
            cache_update(&vc_cache_blocks[i], l1_result->evicted_address, loadstore, 0);
          }
        }
      }
      if (debug) {
        printf("Victim cache ways information:\n");
        print_way_info(0, l1_vc_info->vc_assoc, vc_cache_blocks);
        printf("Operation Result: \n Miss_Hit: %i,\n Dirty_Eviction: %i,\n Evicted_address: %i\n", vc_result->miss_hit, vc_result->dirty_eviction, vc_result->evicted_address);
        printf("################################################################################################");
      }
    }
    else{
        if(debug){
            printf("################################################################################################");
            printf("L1 hit.\n");
            printf("################################################################################################");
        }
    }

    return OK;
  }

}
