/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define LOAD 0
#define STORE 1
#define KB 1024
#define ADDRSIZE 32
using namespace std;

int find_victim(entry* cache_block, int assoc) {
    for(int i = 0; i< assoc; i++) {
        if(cache_block[i].rp_value == assoc -1) {
            return i; 
        }
    }
    return 0;
}


int lru_obl_replacement_policy (int idx,
                                int tag,
                                int associativity,
                                bool loadstore,
                                entry* cache_block,
				                entry* cache_block_obl,
                                operation_result* operation_result_cache_block,
				                operation_result* operation_result_cache_obl,
                                bool debug=false)
{
    int thd;
    int cache_victim;
    int obl_victim;
    if(idx < 0 || tag < 0 || associativity < 1 || !power2(associativity) || cache_block == nullptr || operation_result_cache_block == nullptr || cache_block_obl == nullptr
        || operation_result_cache_obl == nullptr || cache_block_obl == nullptr){
        return PARAM;
    }
    
    for (int i = 0; i < associativity ; i++) {
        /*Valid 0 means theres space in cache*/ 
        if(cache_block[i].valid == 0) {
            //Saves new value 
            cache_update(&cache_block[i], tag, loadstore, -1);
            lru_update(cache_block, associativity, i );
            
            //Sets obl tag to 0 to trigger A+1 block prefetch
            cache_block[i].obl_tag = 0;
            //no need to check condition since the miss is from valid being zero
            cache_update(&cache_block_obl[i], tag, loadstore, -1);
            lru_update(cache_block_obl, associativity, i);
            
            //Save A+1 block in obl cache and set obl_tag to 1 to trigger prefetch if it was hit.
            cache_block_obl[i].obl_tag = true;
            //Results 
            operation_result_cache_block->miss_hit = (loadstore == LOAD) ? MISS_LOAD:MISS_STORE; 
            if(debug) {
                printf("Printing Cache\n");
                print_way_info(idx, associativity, cache_block);
                printf("Printing Obl Cache\n");
                print_way_info(idx, associativity,cache_block_obl);
            }
            return OK; 
        }
        else {
            if(cache_block[i].rp_value == associativity -1) {
                cache_victim = i;
            }
            if(cache_block_obl[i].rp_value == associativity -1) {
                obl_victim = i;
            }
            if(cache_block[i].tag == tag) {
                thd = cache_block[i].rp_value; 
                lru_update(cache_block, associativity, thd);
                cache_update(&cache_block[i], tag, loadstore, 0);
                if(debug) {
                  printf("Printing Cache on a hit\n");
                  print_way_info(idx, associativity, cache_block);
                  
                }   
                //If obl_tag was not cero, prefetch 
                if(cache_block[i].obl_tag == 1){
                    cache_block[i].obl_tag = 0;
                    cache_update(&cache_block_obl[find_victim(cache_block_obl,associativity)], tag, loadstore, -1);
                    lru_update(cache_block_obl, associativity, associativity-1);  
                    cache_block_obl[i].obl_tag = 1;
                    cache_block_obl[i].dirty = false; 
                    if(debug) {
                        printf("Printing Obl Cache after prefetch\n");
                        print_way_info(idx, associativity,cache_block_obl);
                    }
                } 
                operation_result_cache_block->miss_hit = (loadstore == LOAD) ? HIT_LOAD:HIT_STORE;
                return OK; 
            } 
            
        }
    }
    //If we reach here cache doesnt have space and its a miss
    int victim_loc = find_victim(cache_block, associativity);
    cache_update(&cache_block[victim_loc], tag , loadstore, -1);
    lru_update(cache_block, associativity, associativity-1);
    cache_block[victim_loc].obl_tag = false;
    cache_block[victim_loc].dirty = (loadstore == LOAD) ? 0:1;
    victim_loc = find_victim(cache_block_obl, associativity);
    cache_update(&cache_block_obl[victim_loc], tag , loadstore, -1);
    lru_update(cache_block_obl, associativity, associativity -1);
    cache_block_obl[victim_loc].obl_tag = true;
    cache_block_obl[victim_loc].dirty = false;
    //Results
    operation_result_cache_block->miss_hit = (loadstore == LOAD) ? MISS_LOAD:MISS_STORE; 
    if(debug) {
                  printf("Printing Cache on a miss\n");
                  print_way_info(idx, associativity, cache_block);
                  printf("Printing Obl Cache on a miss\n");
                  print_way_info(idx, associativity,cache_block_obl);
                } 
    return OK;
}
