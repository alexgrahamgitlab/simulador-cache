/*
 *  Cache simulation project
 *  Class UCR IE-521
*/

#ifndef L1CACHE_H
#define L1CACHE_H

#include <netinet/in.h>
/*
 * ENUMERATIONS
 */

/* Return Values */
enum returns_types {
 OK,
 PARAM,
 ERROR
};

/* Represent the cache replacement policy */
enum replacement_policy{
 LRU,
 NRU,
 RRIP,
 RANDOM
};

enum miss_hit_status {
 MISS_LOAD,
 MISS_STORE,
 HIT_LOAD,
 HIT_STORE
};

/*
 * STRUCTS
 */

/* Cache entry metadata */

struct entry {
 /* Indicates if the line is valid */
 bool valid;

 /* Indicates if the entry was written */
 bool dirty;

 /* Tag value */
 int tag;

 /* Replacement policy value */
 uint8_t rp_value;

 /* Prefetch tag, only used in OBL */
 bool obl_tag;
};

/* Cache replacement policy results */
struct operation_result {
 /* Result of the operation */
 enum miss_hit_status miss_hit;

 /* Set to one if the evicted line was dirty */
 bool dirty_eviction;

 /* Block address of the evited line */
 int  evicted_address;
};

/* Cache params */
struct cache_params {
  /* Total size of the cache in Kbytes */
  int size;

  /* Number of ways of the cache */
  int asociativity;

  /* Size of each cache line in bytes */
  int block_size;
};

/* Address field size */
struct cache_field_size {
  /* Number of bits used for the tag */
  int tag;

  /* Number of bits used for the idx */
  int idx;

  /* Number of bits used for the offset */
  int offset;
};

/*
 *  Functions
 */

/*
 * Get tag, index and offset length
 *
 * [in] cache_parms :      Cache size, asociativity and block size
 *
 * [out] cache_field_size: Struct containing tag, idx and offset size
 */
int field_size_get(struct cache_params cache_params,
                   struct cache_field_size *field_size);

/*
 * Get tag and index from address
 *
 * [in] address:    memory address
 * [in] field_size: Struct containing tag, idx and offset size in bits
 *
 * [out] idx: cache line idx
 * [out] tag: cache line tag
 */

void address_tag_idx_get(long address,
                         struct cache_field_size field_size,
                         int *idx,
                         int *tag);


/*
 * Search for an address in a cache set and
 * replaces blocks using SRRIP(hp) policy
 *
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation false if load true if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */
int srrip_replacement_policy (int idx,
                              int tag,
                              int associativity,
                              bool loadstore,
                              entry* cache_blocks,
                              operation_result* operation_result,
                              bool debug=false);

/*
 * Search for an address in a cache set and
 * replaces blocks using LRU policy
 *
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation false if true if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */
int lru_replacement_policy (int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug=false);
/*
 * Search for an address in a cache set and
 * replaces blocks using NRU policy
 *
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation false if true if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */

int nru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug=false);

/*
 *  True if num is a power of two.
 * [in] num: integer number to operate
 */
 
bool power2(int num);

/*
 * Update a way of a cache block
 * [in] cache_block: pointer to the cache arrangement
 * [in] tag: tag field of the block
 * [in] loadstore: type of operation false if true if store
 * [in] rp_value: replacement policy value
 */
 
void cache_update(entry* cache_block, int tag, int loadstore, int rp_value);

/*
 * Update the rp_value for all ways in lru policy.
 *
 * [in] cache_block: pointer to the cache arrangement
 * [in] associativity: number of ways of the entry
 * [in] thd: thershold to select which ways has to be updated
 */
 
 int srrip_update(entry* cache_blocks, 
                  int associativity, 
                  int rrip_bits, 
                  int set_loc,
                  int flag_hm);

/*
 * Updates the rp_value for all ways in srrip policy after a miss
 *
 * [out] victim: returns the address of the victim
 * [in] cache_block: pointer to the cache arrangement
 * [in] associativity: number of ways of the entry
 * [in] rrip_bits: determines value of M 
 * [in] set_loc: determines where the set starts
 * [in] flag_hm: flag that indicates hit (1) or miss (0)
 */
 
void print_ways(entry* cache_blocks, int associativity, int set_loc);

/*
 * Prints out each way, tag, and rp_value for the current cache
 *
 * [in] cache_block: pointer to the cache arrangement
 * [in] associativity: number of ways of the entry
 * [in] set_loc: determines where the set starts
 */

void lru_update(entry* cache_block, int associativity, int thd);


/*
 * Update the rp_value for all ways in ssrip policy.
 *
 * [in] cache_block: pointer to the cache arrangement
 * [in] associativity: number of ways of the entry
 * [in] thd: thershold to select which ways has to be updated
 */
 
void srrip_update(entry* cache_blocks, 
                  operation_result* result, 
                  int associativity, 
                  int way_value, 
                  int tag, 
                  int rrip_bits, 
                  int set_loc,
                  int flag_hm,
                  int loadstore);
/*
 * Update the rp_value for all ways in nru policy.
 *
 * [in] cache_block: pointer to the cache arrangement
 * [in] associativity: number of ways of the entry
 * [in] way_value: pointer to way
 * [in] tag: tag field of the block
 * [in] rrip_bits: indicates max value of rp_value which can be 1 if associativity<3 and 3 if associativity=>4
 * [in] set_loc: pointer to the first way of the current set
 * [in] flag_hm: 1 indicates hit, 0 indicates miss
 * [in] loadstore:
 */

void nru_update(entry* cache_blocks, int associativity);


#endif
/*
 * Invalidates a line in the cache
 *
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: cache entry to edit
 *
 * return error if tag is not found in cache blocks
 */
int l1_line_invalid_set(int tag,
                        int associativity,
                        entry* cache_blocks,
                        bool debug);
