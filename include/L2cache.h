/*
 *  Cache simulation project
 *  Class UCR IE-521
 */
#ifndef L2CACHE_H
#define L2CACHE_H
#include "L1cache.h"
/*
 * STRUCTS
 */

/* L1 and l2 info */
struct l1_l2_entry_info {
	int l1_tag;
        int l1_idx;
	int l1_associativity;
	int l2_tag;
        int l2_idx;
	int l2_associativity;
};

/* 
 *  Functions
 */

/*
 * Updates l1cache when there is a hit .
 * [in] cache_to_update, recieves cache to update.
 * [in] tag_in, tag value to find in way
 * [in] loadstore, says if its a load or store, load = 0 , store =1 
 * [in] set indicates the set where the hit is found
 * [in] l1_asoc indicates l1cache asociativity
 * [in] entry_loc, indicate the exact entry location in the cache to update
 * 
*/
void l1cache_hit_update(entry* cache_to_update, int tag_in, int loadstore, int set, int l1_asoc);

/* Used to update l2 cache when theres a hit. 
 * [in] l2cache, recieves l2cache where there was a hit
 * [in] tag_find, tag where there is a hit 
 * [in] loadstore, see if its a load or a store to update dirty bit in l2cache
 * [in] set: indicates the set where the hit is
 * [in] l2_asoc: asoc value for the l2 cache.
*/
int update_l2_hit(entry* l2cache, int tag_find, int loadstore, int set, int l2_asoc);

/** 
 * Function used when either cache has a way with valid == 0.
 * [in] cache: recieves which cache to update be it l1 or l2
 * [in] tag_in; tag to save in the cache
 * [in] loadstore
 * [in] rp_value: says the rp_value to add to the added tag, usually 0.
 * [in] set: set location in the cache to add tag, generally 0 for tests
 * [in] cache_asoc: asociativity of the cache be it l1 or l2.
 * 
*/

/**
 * Function used when the victim location is identified, so no need to find it
 * [in] cache: cache to update be it l1 or l2
 * [in] tag_in: tag to save on the victim way
 * [in] loadstore: used to modify dirtybit.
 * [in] set: indicates the set location in the cache
 * [in] victimloc: indicates the exact way where to write the new info. 
 *  
 */
int victimize(entry* cache, int tag_in, int loadstore, int set, int victimloc);


int cacheupdate(entry* cache, int tag_in, int loadstore, int rp_value, int set, int cache_asoc);
/*
 * Updates the lru rp_values for a given set of ways in the cache 
 * [in] cache_block, recieves the cache to update, can be l1 or l2. 
 * [in] associativity, indicates the amount of lines in a set
 * [in] thd threshold used to indicate which ways have to be updated.
 * [in] idx index says what set in the cache requires to be updated with lru. 
 * 
*/
void l1_l2_lru_update(entry* cache_block, int associativity, int thd, int idx);
/* 
 * Get l1 and l2 entry info.
 * For a given addres and cache characteristics return L1 and L2 entry info, meaning
 * tag, index and associativity
 * 
 * [in] l2_cache params: cache_param struct with l2 info (this is a ro parameter) 
 * [in] l1_cache params: cache_param struct with l1 info (this is a ro parameter) 
 * [in] address:         memory address
 * [in] debug:           if set to one debug information is printed
 *
 * [in/out] l1_l2_entry_info: struct with tag,idx and associativity for L1 and L2 
 */
int l1_l2_entry_info_get (const struct cache_params l1_params,
                          const struct cache_params l2_params,
			  long address,
                          struct l1_l2_entry_info* l1_l2_info,
			  bool debug=false);
/* 
 * Search for an address in a cache set and
 * replaces blocks using LRU policy and a updates L2 cache 
 * 
 * [in] l1_l2_info: struct with l2 and l1 entry info (this is a ro parameter) 
 * [in] loadstore: type of operation true if load false if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] l1_cache_blocks: l1 cache line to work on 
 * [in/out] l2_cache_blocks: l2 cache line to work on
 * [in/out] l1_result: return the cache operation result in l1(miss_hit_status)
 * [in/out] l2_result: return the cache operation result in l2 (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */
int lru_replacement_policy_l1_l2(const l1_l2_entry_info *l1_l2_info,
				 bool loadstore,
				 entry* l1_cache_blocks,
				 entry* l2_cache_blocks,
				 operation_result* l1_result,
				 operation_result* l2_result,
				 bool debug=false);
#endif
