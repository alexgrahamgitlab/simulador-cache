/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>

class L1cache : public ::testing::Test{
    protected:
	int debug_on;
	virtual void SetUp()
	{
           /* Parse for debug env variable */
	   get_env_var("TEST_DEBUG", &debug_on);
	};
};

/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_srrip){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};
  int j;
  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  struct entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on, Checking miss operation);
  for (int j = 0 ; j < 2; j++){
    /* Load operation for j = 0, store for j =1 */
    loadstore = (bool)j;
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    
    status = srrip_replacement_policy(idx, 
                                     tag,   
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on, Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_lru) {
  int status; 
  int i;
  int j;
  int idx; 
  int tag; 
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1; 
  bool debug = 0;
  struct operation_result result = {};

  /*Fill ramdom entry*/
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if(debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  /*Cache a probar */
  struct entry cache_line[associativity];
  for (int j = 0 ; j < 2; j++){
    /* Load operation for j = 0, store for j =1 */
    loadstore = (bool)j;
    for ( i =  0; i < associativity; i++) {
      /* Se llenan la entradas del cache con informacion de prueba*/
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = i;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    
    status = lru_replacement_policy(idx, 
                                     tag,   
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    // Se espera status = 0 para que el return type sea OK.
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    // Se revisa cual tipo de miss esperara del cache. 
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  //Se revisa ahora el hit por store y load. 
  DEBUG(debug_on, Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}

/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_nru) {
  int status; 
  int i;
  int j;
  int idx; 
  int tag; 
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1; 
  bool debug = 0;
  struct operation_result result = {};

  /*Fill ramdom entry*/
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if(debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  /*Cache a probar */
  struct entry cache_line[associativity];
  for (int j = 0 ; j < 2; j++){
    /* Load operation for j = 0, store for j =1 */
    loadstore = (bool)j;
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      /* Se llenan la entradas del cache con informacion de prueba*/
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = 1;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    status = nru_replacement_policy(idx, 
                                     tag,   
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    // Se espera status = 0 para que el return type sea OK.
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    // Se revisa cual tipo de miss esperara del cache. 
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  //Se revisa ahora el hit por store y load. 
  DEBUG(debug_on, Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }


}
/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy 
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */
TEST_F(L1cache, promotion){
  int i;
  int rand_policy = rand()%3;
  int associativity =   1 << (rand()%4);
  int idx;
  int tag; 
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};
  int status; 
  int inserted_block_location;
  int tag_compare;
  /* Fill random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  if(debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  struct entry cache_line[associativity];
  //Se llena el cache_line
  DEBUG(debug_on, Checking promotion operation);
  for (i = 0; i < associativity; i++) {
    cache_line[i].valid = true;
    cache_line[i].tag = rand()%4096;
    cache_line[i].dirty = 0;
    //caso LRU 
    if(rand_policy == 0) {
      cache_line[i].rp_value = i;
    }
    //caso NRU 
    if(rand_policy == 1) {
      cache_line[i].rp_value = 1;
    }
    //caso ssrip
    if(rand_policy == 2) {
      cache_line[i].rp_value = (associativity <= 2)? 1:3;
    }
    while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
    }
  }
  //No es necesario
  //tag = rand()%4096; 
  //Se ingresa un nuevo bloque al cache por medio de un miss.
  if(rand_policy == 0) {
    loadstore = 0;
    printf("\n Lru policy \n");
    //Add new block
    status = lru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));

    for (i = 0 ; i < associativity ; i++) {
      if (cache_line[i].tag == tag) {
        inserted_block_location = i; 
        tag_compare = cache_line[i].tag;
      } else {
      }
    }     
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.miss_hit, MISS_LOAD);
    //Force a hit on the block just entered.
    DEBUG(debug_on, Checking hit promotion LRU);
    status = lru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    EXPECT_EQ(result.miss_hit, HIT_LOAD);
    EXPECT_EQ(status, 0);
    
    EXPECT_EQ(cache_line[inserted_block_location].rp_value, 0);
    //Inserting new blocks 
    i = 0;
    while(i < associativity) {
      tag = rand()%4096;
      status = lru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
      if(result.miss_hit == MISS_LOAD) {
        i++;
        if(result.evicted_address == tag_compare) {
          EXPECT_EQ(status, 0);
          //revisar si el -1 tiene sentido.
          //bloque se debe reeemplazr luego de N-1 bloques
          EXPECT_EQ(i, associativity);
          break;
        }
      }
      
    }

  }
  if(rand_policy == 1) {
    loadstore = 0;
    printf("\n Nru policy \n");
    //Add new block
    status = nru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));

    for (i = 0 ; i < associativity ; i++) {
      if (cache_line[i].tag == tag) {
        inserted_block_location = i; 
        tag_compare = cache_line[i].tag;
      } else {
      }
    }     
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.miss_hit, MISS_LOAD);
    //Force a hit on the block just entered.
    DEBUG(debug_on, Checking hit promotion NRU);
    status = nru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    EXPECT_EQ(result.miss_hit, HIT_LOAD);
    EXPECT_EQ(status, 0);
    
    EXPECT_EQ(cache_line[inserted_block_location].rp_value, 0);
    //Inserting new blocks 
    i = 0;
    while(i < associativity) {
      tag = rand()%4096;
      status = nru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
      if(result.miss_hit == MISS_LOAD) {
        i++;
        if(result.evicted_address == tag_compare) {
          EXPECT_EQ(status, 0);
          //revisar si el -1 tiene sentido.
          //bloque se debe reeemplazr luego de N-1 bloques
          EXPECT_EQ(i, associativity);
          break;
        }
      }
      
    }
  }

  if(rand_policy == 2) {
    loadstore = 0;
    printf("\n Srrip policy \n");
    //Add new block
    status = srrip_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    inserted_block_location = result.evicted_address;
    DEBUG(debug_on, Checking hit promotion SRRIP);
    for (i = 0 ; i < associativity ; i++) {
      if (cache_line[i].tag == tag) {
        inserted_block_location = i; 
        tag_compare = cache_line[i].tag;
      } else {
      }
    }  
    //Force a hit on the block just entered.
    status = srrip_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    EXPECT_EQ(cache_line[inserted_block_location].rp_value, 0);
    //Inserting new blocks
    i = 0; 
    int block_counter = 0;//Block counter
    int expected_misses = (associativity <= 2)?associativity:(associativity-1)*3+1;
    bool done = true;
    while(done) {
      if(i >= associativity) {
        i = 0;
      }
      tag = rand()%4096;
      while(cache_line[i].tag == tag) {
        tag = rand()%4096;
      }
      status = srrip_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
      if(result.miss_hit == MISS_LOAD) {
        //Another block inserted
        i++;
        block_counter++;
        //printf("blockcounter %d\n", block_counter);
        if(result.evicted_address == tag_compare) {
          //bloque se debe reeemplazar en 
          EXPECT_EQ(block_counter, expected_misses);
          done = false; 
          break;
        }
      }
    }
  }
}


/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */
TEST_F(L1cache, writeback){
  int status; 
  int idx; 
  int tag; 
  int associativity; 
  bool loadstore = 0;
  bool debug = 0;
  int rand_policy = rand()%3;
  enum miss_hit_status expected_miss_hit;
  struct operation_result result = {};
  int tag_to_force_hit = 0;
  int tag_to_force_read;
  int i;
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if(associativity < 2) {
    associativity = 2;
  }
  if(debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  struct entry cache_line[associativity];
  DEBUG(debug_on, Checking writeback condition);
  //Se llena el set con datos al azar, luego se revisa cual politica de reemplazo es para llenar su rp_value
  for (i = 0; i < associativity; i++) {
    cache_line[i].valid = true;
    cache_line[i].tag = rand()%4096;
    cache_line[i].dirty = 0;
    //caso LRU 
    if(rand_policy == 0) {
      cache_line[i].rp_value = i;
    }
    //caso NRU 
    if(rand_policy == 1) {
      cache_line[i].rp_value = 0;
    }
    //caso ssrip
    if(rand_policy == 2) {
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
    }
    while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
    }
  }
  tag_to_force_hit = cache_line[0].tag;
  tag_to_force_read = cache_line[1].tag;
  //Teniendo un tag que se sabe es un hit se fuerza un write
  loadstore = 1;
  tag = tag_to_force_hit;
  //Se revisa cual politica de reeemplazo probar
  if(rand_policy == 0) {
    printf("Writeback Lru\n");
    //Force write on block A hit
    status = lru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    //Luego se fuerza un hit de read en otro bloque B
    EXPECT_EQ(result.miss_hit, HIT_STORE);
    tag = tag_to_force_read; 
    loadstore = 0;
    status = lru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.miss_hit, HIT_LOAD);
    //Force read hit of BLOCK A.
    tag = tag_to_force_hit;
    status = lru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    EXPECT_EQ(result.miss_hit, HIT_LOAD);
    i = 0;
    for (i = 0 ; i <associativity ; i++) {
      tag = rand()%4096;
      while(cache_line[i].tag == tag) {
        tag = rand()%4096;
      }
      status = lru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
      if(result.evicted_address == tag_to_force_read) {
        EXPECT_EQ(result.dirty_eviction, 0);
      }
      if(result.evicted_address == tag_to_force_hit) {
        EXPECT_EQ(result.dirty_eviction, 1);
      }
    }
  }
  //NRU poricy
  if(rand_policy == 1) {
    printf("Writeback Nru\n");
    //Force write on block A hit
    status = nru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    //Luego se fuerza un hit de read en otro bloque B
    EXPECT_EQ(result.miss_hit, HIT_STORE);
    tag = tag_to_force_read; 
    loadstore = 0;
    status = nru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.miss_hit, HIT_LOAD);
    //Force read hit of BLOCK A.
    tag = tag_to_force_hit;
    status = nru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    EXPECT_EQ(result.miss_hit, HIT_LOAD);
    i = 0;
    for (i = 0 ; i <associativity ; i++) {
      tag = rand()%4096;
      while(cache_line[i].tag == tag) {
        tag = rand()%4096;
      }
      status = lru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
      if(result.evicted_address == tag_to_force_read) {
        EXPECT_EQ(result.dirty_eviction, 0);
      }
      if(result.evicted_address == tag_to_force_hit) {
        EXPECT_EQ(result.dirty_eviction, 1);
      }
    }
  }
  //srrip policy check
  if(rand_policy == 2) {
    printf("Writeback Srrip\n");
    //Force write on block A hit
    status = srrip_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    //Luego se fuerza un hit de read en otro bloque B
    EXPECT_EQ(result.miss_hit, HIT_STORE);
    tag = tag_to_force_read; 
    loadstore = 0;
    status = srrip_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.miss_hit, HIT_LOAD);
    //Force read hit of BLOCK A.
    tag = tag_to_force_hit;
    status = srrip_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    EXPECT_EQ(result.miss_hit, HIT_LOAD);
    i = 0;
    for (i = 0 ; i <associativity ; i++) {
      tag = rand()%4096;
      while(cache_line[i].tag == tag) {
        tag = rand()%4096;
      }
      status = srrip_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
      if(result.evicted_address == tag_to_force_read) {
        EXPECT_EQ(result.dirty_eviction, 0);
      }
      if(result.evicted_address == tag_to_force_hit) {
        EXPECT_EQ(result.dirty_eviction, 1);
      }
    }
  }
}

/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy 
 * 2. Choose invalid parameters for idx, tag and asociativity
 * 3. Check function returns a PARAM error condition
 */
TEST_F(L1cache, boundaries){
  int status; 
  int idx;
  int tag; 
  int associativity;
  bool loadstore = 0;
  bool debug = 0;
  int rand_policy = 2;
  struct operation_result result = {};
  //Preguntar que tipo de parametros son invalidos.
  idx = -2000;
  tag = 7000;
  associativity = 100;
  /*Se crea el cache*/
  DEBUG(debug_on, Testing boundaries.... );
  struct entry cache_line[associativity];
  if(rand_policy == 0) {
    DEBUG(debug_on, LRU POLICY);
    status = lru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    // Se espera PARAM error;  
    EXPECT_EQ (status, 1);
  }
  if(rand_policy == 1) {
    DEBUG(debug_on, NRU POLICY);
    status = nru_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    // Se espera PARAM error;  
    EXPECT_EQ (status, 1);

  }
  if(rand_policy == 2) {
    DEBUG(debug_on, SSRIP POLICY);
    status = srrip_replacement_policy(idx, 
                            tag,   
                            associativity,
                            loadstore,
                            cache_line,
                            &result,
                            bool(debug_on));
    // Se espera PARAM error;  
    EXPECT_EQ (status, 1);
  }
}

